/*
import the following:
	React from react

	Jumbotron from react-bootstrap/Jumbotron
	Button from react-bootstrap/Button
	Row from react-bootstrap/Row
	Col from react-bootstrap/Col
*/
// Dependencies
import React from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { Link } from 'react-router-dom';


export default function Banner( { data } ) {

    console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}
