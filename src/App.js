// Base Imports
import React, { useState } from 'react';
// import { BrowserRouter, Route, Switch } from 'react-router-dom'; - Switch is depricated that's why we need to use Routes function inside react-router-dom instead
/* 
	react-router-dom allows us to stimulate changing pages in react. Because by default, react is used for SPA - Single Page Application 

	Router - used to wrap components that uses react-router-dom and allows the use of routes and the routing system

	Routes - holds all the Route components (similar to the depricated Switch)

	Route - assigns an endpoint and displays the appropriate page component for that endpoint.
				- path attribute - assigns the endpoint
				-element attribute - assigns the Page component to be displayed at that endpoint (new update)
*/

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// App Imports
import UserContext from './userContext.js';

// App Components
/*
  every component has to be imported before it can be rendered inside the index.js
*/
import AppNavbar from './components/AppNavbar.js';

// Page Components
import Home from './pages/Home.js';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error'


export default function App() {
// localStorage.getItem - used to retrieve a piece of the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.setItem in the Login.js
	const [ user, setUser ] = useState( { email: localStorage.getItem('email') } );

	const unsetUser = () => {
		localStorage.clear();
		setUser( { email: null } )
	}



/*
	path="*" - all other unspecified routes. this is to make sure that all other routes, beside the ones in the return statement, will render the Error page.
*/

/*
	The Provider component inside useContext is what allows other components to consume or use the context. Any component which is not wrapped by the Provider will have access to the values provided in the context.
*/

	return (
		<UserContext.Provider value = {{ user, setUser, unsetUser }}>
			 <Router>
			   <AppNavbar user={user} />
			   <Routes>
			       <Route path = "/" element={<Home />} />
			       <Route path = "/courses" element = {<Courses />} />
			       <Route path = "/register" element = {<Register />} />
			       <Route path = "/login" element = {<Login />} />
			       <Route path="*" element = {<Error />} />
			   </Routes>
			</Router>
		</UserContext.Provider>
		)
}
