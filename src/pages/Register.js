/*
	import:
		react, useState, useEffect from react

		Form, Container, Button from react-bootstrap
*/
// Base Imports
import	React, { useState, useEffect, useContext } from 'react';
import 	{ Navigate } from 'react-router-dom';

// App Imports
import UserContext from '../userContext.js'

// Bootstrap
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2 to install sweetalert2

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ passwordConfirm, setPasswordConfirm ] = useState('');
	const [ isDisabled, setIsDisabled ] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if ( isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ email, password, passwordConfirm ]);

	if (user.email !== null){
		// return <Redirect to="/" /> - Redirect is depricated function inside React and it is replaced by Navigate
		/*
			Navigate - allows us to redirect the users after logging in and updating the global user state. even if the user tru=ies to input the /login as URI, it would still redirect him/her to the home page
				replace to attribute - to specify the page/uri to where the user/s will be redirected
		*/
		return <Navigate replace to to="/courses" />
	}


/*
	TWO_WAY BINDING

		To be able to capture/save the input value from the input elements, we can bind the value of the element with the states. We, as devs, cannot type into the the inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that is bound to the input

		Two-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we don't have to save it before submitting.

		"e.target.value"
		e - the event to which the element will listen
		target - the element where the event will happen
		value - the value that the user has entered in that element

*/


	function register(e){
		e.preventDefault();

		Swal.fire('Register successful, you may now log in.')

		// clears the input fields since they update their respective variable values into an empty string
		setEmail('');
		setPassword('');
		setPasswordConfirm('');
	}


	return(
			<Container fluid>
				<Form onSubmit={register}>
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm password</Form.Label>
						<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
					</Form.Group>
					<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
		)
}
